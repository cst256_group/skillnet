<?php
/* CST-256 Database Application Programming III
 * Milestone 1
 * RegistrationController, Version 1
 * Group CLC Project
 * 09/15/2019
 * This controller is used to register new users.
 */
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function Create(){
        return view('registration.create');
    }
    
    
    public function store()
    {
        $this->validate(request(), [
            'username' => 'required',
            'fName' => 'required',
            'lName' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $user = User::create(request(['username', 'fName', 'lName', 'email', 'password']));
        
        auth()->login($user);
        
        return redirect()->to('/index');
    }
}
