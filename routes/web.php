<?php
/* CST-256 Database Application Programming III
 * Milestone 1
 * web, Version 1
 * Group CLC Project
 * 09/15/2019
 * This is used to support routing for our application.
 */
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','LoginController@Create');
Route::get('/login','LoginController@Add');
Route::get('/login','LoginController@Remove');

Route::get('/register','RegistrationController@Create');
Route::get('/register','RegistrationController@Add');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
